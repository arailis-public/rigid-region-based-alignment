## Overview 
The mediassist3_region_register package was created for the rigid registration of a preoperative model of a liver (the *source cloud*) and an intraoperative liver model (*target cloud*).  
The registration is performed using roughly matchin regions of the source and target cloud (*source regions* and *target regions*), as specified manually by a user.

![Screenshot of GUI](resources/images/RegionRegister.png)

This is an implementation of a paper by Clements et al. [1].

## Usage
The intended use of the tool is within a ROS pipeline, but it can also be used as a standalone GUI tool.

### ROS usage:
The file path to the preoperative point cloud (which doesn't change during an operation) is given at startup via the `--source` command line argument. Alternatively, it can be collected from the ROS parameter server (the tool checks for the parameter "preoperative_meshes/liver_surface/filename").

Example of how to run:
```bash
# If the source filename is set as a parameter on the ROS parameter server:
rosrun mediassist3_region_register region_register
# or:
rosrun mediassist3_region_register region_register --source /path/to/source_cloud.pcd
```

The intraoperative point cloud is received via the topic "/dense_map/points2". The tool listenes to this topic continuously, until the user clicks "Begin/Restart Registration", at which point the point cloud is kept fixed.

### Standalone usage
Alternatively, the tool works without any ROS pipeline active. In this case, simply specify both `--source [FILENAME]` and `--target [FILENAME]` parameters:

```bash
rosrun mediassist3_region_register region_register --source /path/to/source_cloud.pcd --target /path/to/target_cloud.pcd
```
Note that this may hang at the beginning while it's trying to connect to the ros master. Either start `roscore`, or CTRL+C once in the console in which you started region_register.

If regions have already been specified by some other application, you can use the `tool` command (without a GUI), which only runs the registration:
```bash
rosrun mediassist3_region_register tool src_cloud.pcd trgt_cloud.pcd iterationCount errorValue src_reg0.pcd trgt_reg0.pcd src_reg1.pcd trgt_reg1.pcd...`
```

## Installation

The package requires ROS noetic [2] to receive unregistered and send registered data, PCL [3] for the registration algorithms and visualization and Qt [4] for the GUI.  

To install this package, do the following steps:
* Pull this repository into the src directory of your catkin workspace (usually catkin_ws/src or similar)
* Build the package using `catkin_make`.
* Run the package with `rosrun`, e.g. `rosrun mediassist3_region_register region_register input:=/dense_map/points2 path/to/your/example.pcd`.

[1] Clements, L.W., Chapman, W.C., Dawant, B.M., Galloway, R.L., Jr. and Miga, M.I. (2008), Robust surface registration using salient anatomical features for image-guided liver surgery: Algorithm and validation. Med. Phys., 35: 2528-2540. https://doi.org/10.1118/1.2911920

[2] http://wiki.ros.org/

[3] https://pointclouds.org/

[4] https://www.qt.io/
