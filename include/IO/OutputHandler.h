#pragma once

#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/io/pcd_io.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <pcl_ros/point_cloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>

#include "../RegionContainer.h"

#include "yaml-cpp/yaml.h"

// This class handles the output of the application, which means it handles
// the sending and saving of the registered cloud and transformation.
class OutputHandler {
public:
    OutputHandler();
    ~OutputHandler();
    
    // Set the clouds and transformation
    void setCloudsAndTransformation(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source,
        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> t_regions_src,
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4 newTransformation
    );
    
    // Send the transformed preoperative cloud and transformation
    void sendData();
    
    // Save the clouds and transformation
    void saveData(
        std::string filePath,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr output_target_cloud,
        std::vector<RegionContainerRef> &regContainers
    );
    
private:
    
    YAML::Node createYAML();
    
    // The transformation matrix that will be send and stored
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB, pcl::PointXYZRGB>::Matrix4 transformation;
    
    // Source cloud for sending and storing
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr output_source_cloud;
    
    // Vector containing transformed source regions to be sent
    std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> transformed_regions_src;
    
    // Node handle and publisher used to publish the transformation and point cloud
    ros::NodeHandle nh;
    ros::Publisher pub;
};
using OutputHandlerRef = std::shared_ptr<OutputHandler>;
