#pragma once

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>

class RegionContainer {
    
public:
    RegionContainer();
    ~RegionContainer();
    // Add/Remove points of a point cloud to a region
    void addPointsToRegion(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointsToAdd, bool isSource);
    void removePointsFromRegion(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointsToRemove, bool isSource);

    // Set the kd tree for the target region
    void setTree();
    
    // Sets the unlabeled regions of the container
    void setUnlabeledRegions();

    // point cloud getter
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr getSourceRegion() { return source_region; }
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr getTargetRegion() { return target_region; }
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr getIndexedSourceRegion() { return indx_source_region; }
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr getIndexedTargetRegion() { return indx_target_region; }
    
    // Create a clone out of an instance
    std::shared_ptr<RegionContainer> clone();

    // Kd trees for the intraoperative region
    pcl::KdTreeFLANN<pcl::PointXYZRGB> treeTrgt;

private:
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_region;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_region;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_source_region;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_target_region;
};
using RegionContainerRef = std::shared_ptr<RegionContainer>;
