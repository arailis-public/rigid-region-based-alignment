#include "../include/RegionContainer.h"
#include "../include/Utils.h"

RegionContainer::RegionContainer(
) :
    source_region(new pcl::PointCloud<pcl::PointXYZRGB>()),
    target_region(new pcl::PointCloud<pcl::PointXYZRGB>()),
    indx_source_region(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    indx_target_region(new pcl::PointCloud<pcl::PointXYZRGBL>())
{    
}

/**
 * @brief This function adds points to the source or target region of the point
 * @param pointsToAdd The Point cloud which points will be added
 * @param isSource Does the highlighted cloud belong to the source or target one?
 */
void RegionContainer::addPointsToRegion(
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointsToAdd, 
        bool isSource
        ) {
    (isSource ? *indx_source_region : *indx_target_region) += *pointsToAdd;
}

/**
 * @brief This function removes points from a region
 * @param pointsToRemove The Point cloud which points will be removed
 * @param isSource Does the highlighted cloud belong to the source or target one?
 */
void RegionContainer::removePointsFromRegion(
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointsToRemove,
        bool isSource) {
    Utils::removePointsFromCloudByLabel(isSource ? indx_source_region : indx_target_region, pointsToRemove);
}

/**
 * @brief Sets the kd tree
 */
void RegionContainer::setTree() {
    treeTrgt.setInputCloud(target_region);
}

/**
 * @brief Sets the unlabeled source and target regions
 */
void RegionContainer::setUnlabeledRegions() {
    source_region->clear();
    pcl::copyPointCloud(*indx_source_region, *source_region);
    target_region->clear();
    pcl::copyPointCloud(*indx_target_region, *target_region);
}

/**
 * @brief Creates a clone of this region container instance
 */
std::shared_ptr<RegionContainer> RegionContainer::clone() {
    // Normal instance
    RegionContainer rC;
    // Copy point cloud data
    pcl::copyPointCloud(*this->getSourceRegion(), *rC.getSourceRegion());
    pcl::copyPointCloud(*this->getTargetRegion(), *rC.getTargetRegion());
    pcl::copyPointCloud(*this->getIndexedSourceRegion(), *rC.getIndexedSourceRegion());
    pcl::copyPointCloud(*this->getIndexedTargetRegion(), *rC.getIndexedTargetRegion());
    // Return shared ptr instance
    return std::make_shared<RegionContainer>(rC);
}

RegionContainer::~RegionContainer() {
}
