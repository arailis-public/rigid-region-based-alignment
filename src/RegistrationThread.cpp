#include "../include/RegistrationThread.h" 

RegistrationThread::RegistrationThread(
        int iterations, 
        double error,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud,
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_source_cloud,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_cloud,
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_target_cloud,
        const std::vector<RegionContainerRef> &regContainers
    ) :
    // Pass the iteration and error value
    m_iterations(iterations),
    m_error(error),
    // Allocate correspondences and non regions
    correspondences(new pcl::Correspondences()),
    cloud_non_region(new pcl::PointCloud<pcl::PointXYZRGB>()),
    cloud_non_region_labeled(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    // Allocate the copy clouds
    c_target_cloud(new pcl::PointCloud<pcl::PointXYZRGB>()),
    c_indx_target_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    // Allocate the loop cloud
    c_source_cloud(new pcl::PointCloud<pcl::PointXYZRGB>()),
    loop_indx_source_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    // Now all remaining clouds
    regions_src(new pcl::PointCloud<pcl::PointXYZRGB>())
{
    // Now copy all data to the member variables
    pcl::copyPointCloud(*target_cloud, *c_target_cloud);
    pcl::copyPointCloud(*indx_target_cloud, *c_indx_target_cloud);
    pcl::copyPointCloud(*source_cloud, *c_source_cloud);    
    pcl::copyPointCloud(*indx_source_cloud, *loop_indx_source_cloud);
    // Deep copy of region container vectors
    for(auto const &p: regContainers) {
        c_regContainers.push_back(p->clone());
    }
    // Register new metatypes for qt signals
    qRegisterMetaType<pcl::PointCloud<pcl::PointXYZRGB>>("pcl::PointCloud<pcl::PointXYZRGB>");
    qRegisterMetaType<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>("pcl::PointCloud<pcl::PointXYZRGB>::Ptr");
    qRegisterMetaType<std::vector<RegionContainerRef>>("std::vector<RegionContainerRef>");
    qRegisterMetaType<pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4>
        ("pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4");
    qRegisterMetaType<std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>>("std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>");
}

/**
 * @brief Main threading function, executes the registration itself
 */
void RegistrationThread::run() { 
    
    // Final transformation getting values of the translation
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB, pcl::PointXYZRGB>::Matrix4 trans_final = initialAlignment().matrix();
    
    // The first step is to set the trees of the region containers
    for(int i = 0; i < c_regContainers.size(); i++) {
        c_regContainers.at(i)->setTree();
    }

    // Now the indices for the index vectors are set
    setRegionIndices();
    // The cloud containing non region data is filled
    setNonRegionCloud();
    
    // Pass the data of the source regions
    for(int i = 0; i < c_regContainers.size(); i++) {
        *regions_src += *c_regContainers.at(i)->getSourceRegion();
        // Store the source regions in the transformed vector
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr newCloud(new pcl::PointCloud<pcl::PointXYZRGB>());
        *newCloud = *c_regContainers.at(i)->getSourceRegion();
        transformed_regions_src.push_back(newCloud);
    }
    
    // Main loop, iterate for the iteration count selected by user or until a threshold is undercut
    for(int i = 1; i < m_iterations; i++) {
        // Create the correspondences first
        createCorrespondences();
        
        // Then create the transformation estimation
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB, pcl::PointXYZRGB, float> tE;
        // The transformation matrix used for this registration iteration
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4 trans_iter;
        // Estimate and apply transformation
        tE.estimateRigidTransformation(*c_source_cloud, *c_target_cloud, *correspondences, trans_iter);
        // Apply the transformation to the transformation clouds
        pcl::transformPointCloud (*c_source_cloud, *c_source_cloud, trans_iter);
        pcl::transformPointCloud (*loop_indx_source_cloud, *loop_indx_source_cloud, trans_iter);
        pcl::transformPointCloud (*regions_src, *regions_src, trans_iter);
        
        // Apply local transformation to the final one
        trans_final = trans_iter * trans_final;
        
        // Now estimate the error using the transformed point cloud
        float currentError = estimateError();
        // If the current error is smaller then the given error value, we are finished
        if(currentError < m_error) {
            std::cout << "Error minimum undercut. Exiting. (Current error): " << currentError << std::endl;
            break;
        }

        // Readjust the weighting for the error estimation
        adjustDynamicWeighting(i);
        
        // Emit the current iteration, then unlock
        emit updateProgress(i, *c_source_cloud, *regions_src, c_regContainers);
        std::cout << "Iteration: " << i << std::endl;
        std::cout << "Current error: " << currentError << std::endl;
        std::cout << "--------" << std::endl;
    }
    // Use the final transformation to transform the vector regions
    for(int i = 0; i < transformed_regions_src.size(); i++) {
        pcl::transformPointCloud (*transformed_regions_src.at(i), *transformed_regions_src.at(i), trans_final);
    }
    
    // Emit the signal that the registration is finished
    emit regDone(c_source_cloud, transformed_regions_src, trans_final);
    std::cout << "Registration complete." << std::endl;
}

/**
 * @brief Performs an initial alignment, aligning the source cloud to the target one
 * @return the initial aligment affine parameter
 */
Eigen::Affine3f RegistrationThread::initialAlignment() {
    // The affine transformation to be used
    Eigen::Affine3f transform = Eigen::Affine3f::Identity();
    
    // Points containing the average of the point clouds
    pcl::PointXYZRGB cp, ci;
    // Add the values of the corresponding point clouds
    for(int i = 0; i < c_source_cloud->size(); i++) {
        cp.x += c_source_cloud->at(i).x;
        cp.y += c_source_cloud->at(i).y;
        cp.z += c_source_cloud->at(i).z;
    }
    for(int i = 0; i < c_target_cloud->size(); i++) {
        ci.x += c_target_cloud->at(i).x;
        ci.y += c_target_cloud->at(i).y;
        ci.z += c_target_cloud->at(i).z;
    }
    // Calculate the average by dividing with point cloud size
    cp.x /= c_source_cloud->size(); 
    cp.y /= c_source_cloud->size(); 
    cp.z /= c_source_cloud->size(); 
    ci.x /= c_target_cloud->size();
    ci.y /= c_target_cloud->size();
    ci.z /= c_target_cloud->size();

    // Create a transformation and set it's translation parameter
    transform.translation() << ci.x - cp.x, ci.y - cp.y, ci.z - cp.z;
    
    // Translate the source cloud and the region containers
    pcl::transformPointCloud (*c_source_cloud, *c_source_cloud, transform);
    for(int i = 0; i < c_regContainers.size(); i++) {
        pcl::transformPointCloud (*c_regContainers.at(i)->getSourceRegion(), *c_regContainers.at(i)->getSourceRegion(), transform);
    }
    
    return transform;
}

/**
 * @brief Sets the indices in the vectors to provide the information, which region they belong to
 */
void RegistrationThread::setRegionIndices() {
    // Resize the vectors to the point cloud's size
    source_region_indices.resize(c_source_cloud->size());
    target_region_indices.resize(c_target_cloud->size());
    
    // Fill the vectors with zeros
    std::fill(source_region_indices.begin(), source_region_indices.end(), 0);
    std::fill(target_region_indices.begin(), target_region_indices.end(), 0);
    // Every vector index belonging to a region will get a number belonging to this region. Because the number is > 0, we start with 1
    int regionIndex = 1;
    // Now iterate over the region container vector
    for(int i = 0; i < c_regContainers.size(); i++) {
        // Same for the reg container belonging to index i
        for(int j = 0; j < c_regContainers.at(i)->getIndexedSourceRegion()->size(); j++) {
            // Every source region index gets the new value
            source_region_indices.at(c_regContainers.at(i)->getIndexedSourceRegion()->at(j).label) = regionIndex;
        }
        // The same procedure for the vector containing the target indices
        for(int j = 0; j < c_regContainers.at(i)->getIndexedTargetRegion()->size(); j++) {
            target_region_indices.at(c_regContainers.at(i)->getIndexedTargetRegion()->at(j).label) = regionIndex;
        } 
        // Increment the indexer before another run
        regionIndex++;
    }
}

/**
 * @brief Sets the cloud not containing any region points
 */
void RegistrationThread::setNonRegionCloud() {
    // Clear before another run
    cloud_non_region->clear();
    cloud_non_region_labeled->clear();
    // If the indices are not belonging to a region, store them
    for(int i = 0; i < c_indx_target_cloud->size(); i++) {
        if(target_region_indices.at(i) == 0) {
            // Labeled point
            cloud_non_region_labeled->push_back(c_indx_target_cloud->at(i));
            // Unlabeled point
            pcl::PointXYZRGB point;
            pcl::copyPoint(c_indx_target_cloud->at(i), point);
            cloud_non_region->push_back(point);
        }
    }
    // Set the tree of this cloud if it is not empty
    if(!cloud_non_region->empty()) {
        treeSet = true;
        treeNonRegion.setInputCloud(cloud_non_region);
    }
}

/**
 * @brief Creates a correspondence used for the registration
 */
void RegistrationThread::createCorrespondences() {
    correspondences->clear();
    // Iterate the source point cloud
    for(int i = 0; i < c_source_cloud->size(); i++) {
        // Skip if the point is in no region
        if(source_region_indices.at(i) == 0) {
            continue;
        }
        // index of the target cloud for the correspondence to be created
        // Value set to max so that the distance calculation can work properly
        int correspondenceIntra = std::numeric_limits<int>::max();
        // The minimumDistance we are searching for
        float minimumDistance = std::numeric_limits<int>::max();
        // Vectors for the point search
        std::vector<int> pointSearch(1);
        std::vector<float> pointSqrdDist(1);
        
        // Iterate over the region containers and search for the nearest target point for every region
        for(int j = 0; j < c_regContainers.size(); j++) {
            // Find the nearest point
            if (c_regContainers.at(j)->treeTrgt.nearestKSearch(c_source_cloud->at(i), 1, pointSearch, pointSqrdDist) > 0) {
                // Pass the calculated distance to a current value
                float currentDistance = sqrt(pointSqrdDist[0]);
                // If the current index of the source cloud and the current tree are out of the same region, 
                // multiply the current value with the weighting factor
                if(source_region_indices.at(i) == j + 1) {
                    currentDistance *= weightingCorrespondences;
                }
                // If this value is smaller then the minimum distance, set to this value
                if(currentDistance < minimumDistance) {
                    minimumDistance = currentDistance; 
                    // Now hand over the correct index of the region cloud, stored in the label of the indx point
                    correspondenceIntra = c_regContainers.at(j)->getIndexedTargetRegion()->at(pointSearch[0]).label;
                }
            }
        }
        // Repeat for the non region points if the tree has been set
        if(treeSet) {
            if (treeNonRegion.nearestKSearch(c_source_cloud->at(i), 1, pointSearch, pointSqrdDist) > 0) {
                if(sqrt(pointSqrdDist[0]) < minimumDistance) {
                    correspondenceIntra = cloud_non_region_labeled->at(pointSearch[0]).label;
                }
            }
        }
        // Create a new correspondence using the current source index and the estimated target index and store it
        pcl::Correspondence c;
        c.index_query = i;
        c.index_match = correspondenceIntra;
        correspondences->push_back(c);
    }
}

/**
 * @brief Estimate the error of the transformation
 */
float RegistrationThread::estimateError() {
    float error = 0.0f;
    // Apply squared distances to the error
    for(int i = 0; i < correspondences->size(); i++) {
        // Get the points out of the correspondences
        pcl::PointXYZRGB p_t_src  = c_source_cloud->at(correspondences->at(i).index_query);
        pcl::PointXYZRGB p_trgt = c_target_cloud->at(correspondences->at(i).index_match);
        // Calculate the squared distance between the transformed source point and the target point
        float sqrdDistance = pow(p_trgt.x - p_t_src.x, 2) + pow(p_trgt.y - p_t_src.y, 2) + pow(p_trgt.z - p_t_src.z, 2);
        // If the source point belongs to a region, apply the dynamic weighting scheme
        if(source_region_indices.at(i) != 0) {
            sqrdDistance *= weightingDynamic;
        }
        error += sqrdDistance;
    }
    // Apply the square root and return
    return sqrt(error);
}

/**
 * @brief Adjusts the dynamic weighting factor for every iteration during registration
 * @param iteration The current iteration number is used for the weighting
 */
void RegistrationThread::adjustDynamicWeighting(int iteration) {
    weightingDynamic = 1000 * exp(-0.01 * (iteration - 1)) + 25 * (1 - exp(-0.01 * (iteration - 1)));
}

RegistrationThread::~RegistrationThread() {
}
