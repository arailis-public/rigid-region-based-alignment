#include "../include/Utils.h" 

/**
 * @brief Checks if a file is in pcd format and loads it if this is the case
 * @param filename The file path
 * @param source_cloud The cloud which can be filled with points
 */
bool Utils::checkPCD (
        std::string filename,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud
    ) {
    // At first, we check if the file is in pcd format
    if(filename.substr(filename.find_last_of(".") + 1) == "pcd") {
        // If so, try to load it
        if (pcl::io::loadPCDFile (filename, *source_cloud) < 0)  {
            // If the loading was not successful, abort
            return false;
        }
        // Else we are done
        return true;
    }
    // Return false if the file is not in pcd format
    return false;
}

/**
 * @brief Creates a vtkpointset out of a given file
 * @param filename The file path
 */
vtkSmartPointer<vtkPointSet> Utils::convertFile(
        std::string filename
        ) {
    // Check if the file is in stl format
    if(filename.substr(filename.find_last_of(".") + 1) == "stl") {
        // Read the file and convert it
        vtkSmartPointer<vtkSTLReader> reader = vtkSmartPointer<vtkSTLReader>::New();
        reader->SetFileName(filename.c_str());
        reader->Update();
        vtkSmartPointer<vtkPointSet> grid = reader->GetOutput();
        // Return the final vtkpointset
        return grid;
    // Else, check for obj
    } else if(filename.substr(filename.find_last_of(".") + 1) == "obj") {
        vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
        reader->SetFileName(filename.c_str());
        reader->Update();
        vtkSmartPointer<vtkPointSet> grid = reader->GetOutput();
        return grid;
    // vtk file
    } else if(filename.substr(filename.find_last_of(".") + 1) == "vtk") {
        vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
        reader->SetFileName(filename.c_str());
        reader->Update();
        vtkSmartPointer<vtkPointSet> grid = reader->GetOutput();
        return grid;
    }
    // vtu file
    else if(filename.substr(filename.find_last_of(".") + 1) == "vtu") {
        vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
        reader->SetFileName(filename.c_str());
        reader->Update();
        vtkSmartPointer<vtkPointSet> grid = reader->GetOutput();
        return grid;
    }
    // vtp file
    else if(filename.substr(filename.find_last_of(".") + 1) == "vtp") {
        vtkSmartPointer<vtkXMLPolyDataReader> reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
        reader->SetFileName(filename.c_str());
        reader->Update();
        vtkSmartPointer<vtkPointSet> grid = reader->GetOutput();
        return grid;
    }
    // ply file
    else if(filename.substr(filename.find_last_of(".") + 1) == "ply") {
        vtkSmartPointer<vtkPLYReader> reader = vtkSmartPointer<vtkPLYReader>::New();
        reader->SetFileName(filename.c_str());
        reader->Update();
        vtkSmartPointer<vtkPointSet> grid = reader->GetOutput();
        return grid;
    }
    else {
        return nullptr;
    }
}

/**
 * @brief Converts the vtkpointset to a PointXYZRGB cloud
 * @param grid the input vtkpointset
 * @param source_cloud The cloud which will be filled with points
 */
void Utils::createInputCloud(
        vtkSmartPointer<vtkPointSet> grid,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud
        ) {
    // Iterate over the grid points
    for(int i = 0; i < grid->GetNumberOfPoints(); i++) {
        // Get each point coordinate
        double point[3];
        grid->GetPoint(i, point);
        // Store the coordinates in a cloud point
        pcl::PointXYZRGB cloud_point;
        cloud_point.x = point[0];
        cloud_point.y = point[1];
        cloud_point.z = point[2];
        // Store this cloud point in the new cloud
        source_cloud->push_back(cloud_point);
    }
}

/**
 * @brief Manages the filling of the point cloud with data src the file source
 * @param filename The file path
 * @param source_cloud The cloud which will be filled with points
 */
bool Utils::loadPointCloud(
        std::string filename,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud
        ) {
    // First, check if the file is in pcd format so we can load it directly
    if(checkPCD(filename, source_cloud)) {
        // if this is the case, we are finished
        return true;
    }
    // Else, try to convert the file into a vtkpointset
    vtkSmartPointer<vtkPointSet> grid = convertFile(filename);
    // If this was not successful, abort
    if(grid == nullptr) {
        return false;
    }
    // Else transfer the point data to the cloud
    createInputCloud(grid, source_cloud);
    return true;
}

/**
 * @brief copies a pointcloud and assigns each point its original index
 * @param src original cloud data
 * @param trgt cloud to copy to
 */
void Utils::copyPointCloudWithIndex(pcl::PointCloud<pcl::PointXYZRGB>::Ptr src, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr trgt) {
    // Clear before another run
    trgt->clear();
    // Store points in clouds, sync the label index with normal point index
    for(int i = 0; i < src->size(); i++) {
        pcl::PointXYZRGBL indxPoint;
        indxPoint.x = src->at(i).x;
        indxPoint.y = src->at(i).y;
        indxPoint.z = src->at(i).z;
        indxPoint.label = (uint32_t)i;
        trgt->push_back(indxPoint);
    }
}

/**
 * Removes equal points src a labelled point cloud (points are equal if label is equal), both containers are sorted
 * @param src point cloud to remove the points src
 * @param remove points that should be removed
 */
void Utils::removePointsFromCloudByLabel(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr src, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr remove) {
    std::set<unsigned int> labels;

    for (auto& point : remove->points)
    {
        labels.insert(point.label);
    }

    Utils::removeByLabel<pcl::PointXYZRGBL>(src, labels);
}


/**
 * @brief Check the console arguments for a flag for a source or target cloud and load the file if provided
 * @param argc, @param argv command line arguments
 * @param flag the provided flag
 * @param cloud loaded if flag was provided
 */
int Utils::checkForFlag (
    int argc, 
    char *argv[],
    std::string flag,
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud
    ) {
    // Search arguments
    for (int i = 1; i < argc; i++) {  
        // If found, try to load
        if (flag.compare(argv[i]) == 0) {
            // Abort if loading was not successful
            if (!Utils::loadPointCloud(argv[i + 1], cloud)) {
                return -1;
            }
            // Success
            return 1;
        }
    }
    // Flag not found
    return 0;
}



Eigen::Vector3f Utils::projectOntoRay(const Eigen::Vector3f &origin, const Eigen::Vector3f &target, const Eigen::Vector3f &point)
{
    auto ot = target-origin;
    auto op = point-origin;
    return origin + ot.dot(op)/ot.dot(ot)*ot;
}


double* Utils::getResolution(const double* corners, int gridSize) {
    auto* res = new double[3];

    auto xmin = corners[0];
    auto xmax = corners[1];
    auto ymin = corners[2];
    auto ymax = corners[3];
    auto zmin = corners[4];
    auto zmax = corners[5];

    res[0] = (xmax - xmin)/(gridSize-1);
    res[1] = (ymax - ymin)/(gridSize-1);
    res[2] = (zmax - zmin)/(gridSize-1);

    return res;
}

bool Utils::planeIntersection(const Eigen::Vector3f& planeNormal, const Eigen::Vector3f& planePoint,
                              const Eigen::Vector3f& rayOrigin, const Eigen::Vector3f& rayDirection,
                              float& t) {
    Eigen::Vector3f planeNormalN = planeNormal.normalized();
    Eigen::Vector3f rayDirectionN = rayDirection.normalized();

    float denominator = planeNormalN.dot(rayDirectionN);
    auto origDiff = planePoint - rayOrigin;
    if (denominator != 0)
    {
        t = origDiff.dot(planeNormalN) / denominator;
    }

    return (t >= 0);

}
