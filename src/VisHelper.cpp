#include "../include/VisHelper.h"

/**
 * @brief Visualizes the final registration
 * @param t_source_cloud, @param t_regions_src clouds added to the viewer
 * @param regContainers target regions added to the viewer
 * @param viewer used to display the transformed clouds
 */
void VisHelper::visualizeRegistration(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_source_cloud,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_regions_src,
        const std::vector<RegionContainerRef> &regContainers,
        pcl::visualization::PCLVisualizer::Ptr viewer
        ) {
    
    // If the target regions were not added yet, do this
    if(!viewer->contains("target_regions")) {
        // Copy all target regions data into the new cloud
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_regions(new pcl::PointCloud<pcl::PointXYZRGB>());
        for(int i = 0; i < regContainers.size(); i++) {
            *target_regions += *regContainers.at(i)->getTargetRegion();
        }
        // Add it to the viewer
        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> brown(target_regions, 204, 102, 0);
        updateCloud(viewer, target_regions, brown, "target_regions", 9);
    }
    
    // Add a new color to distinguish the transformation cloud
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> blue(t_source_cloud, 0, 153, 255);
        
    if(!viewer->contains("registered_cloud")) {
        viewer->addPointCloud(t_source_cloud, blue, "registered_cloud");
    }
    if(!viewer->contains("t_regions_src")) {
        viewer->addPointCloud(t_regions_src, blue, "t_regions_src");
    }

    // Add the updated clouds to the viewer
    updateCloud(viewer, t_source_cloud, blue, "registered_cloud", 1);
    updateCloud(viewer, t_regions_src, blue, "t_regions_src", 9);
}

/**
 * @brief Clean up some pointclouds after a registration is done
 * @param highl_source_cloud, @param highl_target_cloud clouds that will be cleared
 * @param viewer_left, @param viewer_right - needs to update to remove the marked points and display the stored points
 */
void VisHelper::cleanUp(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr highl_source_cloud,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr highl_target_cloud,
        pcl::visualization::PCLVisualizer::Ptr viewer_left,
        pcl::visualization::PCLVisualizer::Ptr viewer_right
        ) {
    // Clear highlighted points
    highl_source_cloud->clear();
    highl_target_cloud->clear();
    
    // Update viewers
    viewer_left->updatePointCloud(highl_source_cloud, "highl_source_cloud");
    viewer_right->updatePointCloud(highl_target_cloud, "highl_target_cloud");
}

/**
 * @brief Checks if the intraoperative cloud has a color and adds it to a viewer
 * @param viewer viewer to update
 * @param cloud cloud that will be added
 * @param id used to add the point cloud
 */
void VisHelper::checkColorAndAddCloud(
        pcl::visualization::PCLVisualizer::Ptr viewer,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
        std::string id
        ) {
    // If the cloud has no colors in the first point, it has most likely no color at all
    // So it gets a set color. Because the left cloud has a blue color, add a brown color to differentiate
    viewer->removePointCloud(id);
    if(!cloud->at(0).r || !cloud->at(0).g || !cloud->at(0).b) {
        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> brown(cloud, 204, 102, 0);
        viewer->addPointCloud (cloud, brown, id);
    // Otherwise, just add the cloud to the viewer
    } else {
        viewer->addPointCloud (cloud, id);
    }
}
