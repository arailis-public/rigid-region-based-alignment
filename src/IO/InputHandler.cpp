#include "../../include/IO/InputHandler.h"

InputHandler::InputHandler(PCLViewerRef pclV)
    : m_pclV(pclV),
    cloud (new pcl::PointCloud<pcl::PointXYZRGB>()
    ) {
        // Subscribe to the dense maps topic
        sub = nh.subscribe ("/dense_map/points2", 1, &InputHandler::callback, this);
}

InputHandler::~InputHandler() {
}
    
// Callback function, updates the right viewport cloud if the user is in update mode
void InputHandler::callback (const sensor_msgs::PointCloud2ConstPtr& input) {
    // If the user is registering or the cloud is empty, 
    // we don't want updates to the cloud, so just return
    if(m_pclV->registering() || input->width * input->height == 0) {
        return;
    }
    // Clear the cloud for very callback
    cloud->clear();
    // Convert the input cloud and update the target viewport
    pcl::fromROSMsg(*input, *cloud);
    m_pclV->updateTargetCloud(cloud);
}
